import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { Observable } from 'rxjs/Observable';

import { PowersService } from '../../../core/services/powers.service';
import { Power } from '../../../core/models/power.model';
import { AddPowerDialogComponent } from '../../components/add-power-dialog/add-power-dialog.component';
import { Store } from '@ngrx/store';
import { getAllPowers, PowersState } from '../../../state/powers/reducers';
import { DeletePower, LoadPowers } from '../../../state/powers/actions/powers';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  powers: Observable<Array<Power>>;

  constructor(private matDialog: MatDialog,
              private store: Store<PowersState>) {
  }

  ngOnInit() {
    this.powers = this.store.select(getAllPowers);
    this.store.dispatch(new LoadPowers());
  }

  add() {
    this.matDialog.open(AddPowerDialogComponent);
  }

  delete(power: Power) {
    this.store.dispatch(new DeletePower(power));
  }

}
